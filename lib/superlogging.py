import logging


def table_exists(cursor, tablename):
    cursor.execute("SELECT EXISTS (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE n.nspname = 'public' AND c.relname = '{}' AND c.relkind = 'r');".format(tablename))
    tableExists = cursor.fetchone()[0]
    return tableExists

class PostgreSQLHandler(logging.Handler):
    """A Handler Class that logs to a PostgreSQL database"""

    def __init__(self, connection, tablename = 'logging'):
        logging.Handler.__init__(self)
        self.connection = connection
        self.cursor = self.connection.cursor()
        self.tablename = tablename
        if not table_exists(self.cursor, tablename):
            self.cursor.execute("CREATE TABLE {} (id SERIAL UNIQUE, time TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'), level VARCHAR, name VARCHAR, message VARCHAR)".format(self.tablename))

    def emit(self, record):
        try:
            msg = self.format(record)
            self.cursor.execute("INSERT INTO {} (level, name, message) VALUES ('{}', '{}', '{}')".format(self.tablename, record.levelname, record.name, msg.replace('\'', '\"')))
            self.connection.commit()
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            self.handleError(record)

class PrintHandler(logging.Handler):
    """A Handler Class that logs to a PostgreSQL database"""

    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        try:
            msg = self.format(record)
            print(msg)
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            self.handleError(record)