import configparser

def getpin(head, name='pin'):
    parser = configparser.ConfigParser()
    parser.read('./config/pins.ini')
    pi = parser[head]
    return int(pi[name])