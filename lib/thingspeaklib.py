import thingspeak
from urllib.error import HTTPError

def get_channel(head='thingspeak'):
    parser = configparser.ConfigParser()
    parser.read('./config/thingspeak.ini')
    ts = parser[head]
    return thingspeak.Channel(id=ts['id'],write_key=ts['write_key'],api_key=ts['api_key'])