import psycopg2, configparser

def connect(database = '', host = '', port = 0, user = '', password = '', name = 'superlogging'):
    conString = "dbname={} host={} port={} user={} password={} application_name={}".format(database, host, port, user, password, name)
    return psycopg2.connect(conString)

def get_connection(head='pgsql',name = 'superlogging'):
    parser = configparser.ConfigParser()
    parser.read('./config/sql.ini')
    pg = parser[head]
    return connect(database=pg['database'],host=pg['host'],port=int(pg['port']),user=pg['user'],password=pg['password'], name=name)