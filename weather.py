import Adafruit_DHT, optparse, time, logging
from lib import postgrelib, superlogging, thingspeaklib, pinlib
from logging.config import fileConfig

fileConfig('./config/logging_config.ini')
logger = logging.getLogger()


def main(ts=False, dht22=True):
    conn, cursor = postgrelib.get_connection(name='weather', head='pgsql_data')
    channel=None
    if ts:
        channel = thingspeaklib.get_channel('weather')
    if dht22:
        sensor = sensor=Adafruit_DHT.DHT22
    else:
        sensor = Adafruit_DHT.DHT11
    pin = pinlib.getpin('weather')
    running = True
    while running:
        try:
            humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
            if humidity and temperature:
                cursor.execute(
                    "INSERT INTO pi-weather-data (temperature, humidity) VALUES({}, {});".format(temperature, humidity))
                conn.commit()
                response = channel.update({'field1': temperature, 'field2': humidity})
        except thingspeaklib.HTTPError:
            logger.warning('Thingspeak Connection Error')
        except KeyboardInterrupt:
            logger.info('Keyboard Interrupt')
            running = False
        except Exception as e:
            logger.exception(e)


if __name__ == '__main__':
    #logger
    conn, cursor = postgrelib.get_connection(name='weatherlog',head='pgsql_logging')
    handler = superlogging.PostgreSQLHandler(conn, tablename='pi-weather')
    handler.setLevel(logging.WARNING)
    logger.addHandler(handler)
    logger.info('Logging-config loaded')
    #optparse
    parser = optparse.OptionParser()
    parser.add_option('-t','--thingspeak',dest='thingspeak',action='store_true',default=False,help='Activate thingspeak')
    parser.add_option('-d', '--dht11',dest='dht11', action='store_true', default=False, help='use the dht11 sensor instead of the dht22')
    options,args=parser.parse_args()
    #main
    main(ts=options.thingspeak, dht22=not options.dht11)